import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Stef - Hello World!!</h1>
        <h3>Learn React</h3>
      </header>
    </div>
  );
}

export default App;
